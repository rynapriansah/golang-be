package main

import (
	"fmt"
	"github.com/gin-gonic/gin"
	"net/http"
)

var data []Antrian

type Antrian struct {
	Id string `json:"id"`
	Status bool `json:"status"`
}

func main() {
	router := gin.Default()
	router.POST("/api/v1/antrian",AddAntrianHandler)
	router.GET("/api/v1/antrian/status",GetAntrianHandler)
	router.PUT("/api/v1/antrian/:idAntrian",UpdateAntrianHandler)
	router.DELETE("/api/v1/antrian/id: idAntrian/delete",DeleteAntrianHandler)
	router.Run(":8080")
	
}

func AddAntrianHandler(c *gin.Context){
	flag,err := addAntrian()
	if flag {
		c.JSON(http.StatusOK,map[string]interface{}{
			"status":"succes",
			})
	}else {
		c.JSON(http.StatusBadRequest,map[string]interface{}{
			"status": "failed",
			"error":err,
			})
	}
}

func addAntrian() (bool,error){
	_,_,dataAntrian :=getAntrian()
	var Id string

	if dataAntrian == nil{
		id = fmt.Sprintf("0-0")
	}else {
		id = fmt.Sprintf("B-d", len(dataAntrian))
	}
	data =append(data, Antrian {
		Id: Id,
		Status: false,
		})

	return true,nil
}


func GetAntrianHandler (c *gin.Context){

	flag,err,resp := getAntrian()
	if flag {
		c.JSON(http.StatusOK,map[string]interface{}{
			"status":"succes",
			"data": resp,
			})
	}else {
		c.JSON(http.StatusBadRequest,map[string]interface{}{
			"status": "failed",
			"error":err,
			})
	}
}

func getAntrian(bool,error,[]Antrian){
	return true,nil,data
	
}


func UpdateAntrianHandler (c *gin.Context){
	idAntrian := c.Param("idAntrian")
	flag,err := updateAntrian(idAntrian)
	if flag {
		c.JSON(http.StatusOK,map[string]interface{}{
			"status":"succes",
			})
	}else {
		c.JSON(http.StatusBadRequest,map[string]interface{}{
			"status": "failed",
			"error":err,
			})
	}
}

func updateAntrian(idAntrian string) (bool,error) {
	for i,_ :=range data {
		if data[i].id == idAntrian{
			data[i].Status = true
			break
		}
	}
	return true,nil	
}


func DeleteAntrianHandler (c *gin.Context){
	idAntrian := c.Param("idAntrian")
	flag,err :=  deleteAntrian (idAntrian)
	if flag {
		c.JSON(http.StatusOK,map[string]interface{}{
			"status":"succes",
			})
	}else {
		c.JSON(http.StatusBadRequest,map[string]interface{}{
			"status": "failed",
			"error":err,
			})
	}
}

func deleteAntrian (idAntrian string) (bool, error) {
	for i :=range data{
		if data[i].id == idAntrian{
			data = append(data[:i], data[i+1:]...)
		}
	}
	return true,nil
}